;;; pinky-mode.el --- Navigate without holding meta keys.

;; Copyright (C) 2013-2014, Jay Conrod, all rights reserved.

;; Author: Jay Conrod <jayconrod@gmail.com>
;; Description: Navigate without holding meta keys.
;; Keywords: pinky key bindings navigation
;; Version: 1
;; Package-Requires: ((window-number "1.0"))

;; This file is NOT part of GNU Emacs

;; License
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:

;; 1. Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.

;; 2. Redistributions in binary form must reproduce the above
;;    copyright notice, this list of conditions and the following
;;    disclaimer in the documentation and/or other materials provided
;;    with the distribution.

;; 3. Neither the name of the copyright holder nor the names of its
;;    contributors may be used to endorse or promote products derived
;;    from this software without specific prior written permission.

;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


;;; Commentary:
;;
;; "Emacs Pinky" is a condition where you feel pain in one or both of
;; your pinky fingers from using Emacs. This is because Emacs requires
;; you to hold [Ctrl] to do anything other than inserting text. On
;; most keyboards, [Ctrl] is pressed with the pinky, so this causes a
;; lot of stress.
;;
;; What can you do about this though? Switch to Vim? You'll have to
;; learn a new set of key bindings and throw away all your
;; customizations! It will be awful!
;;
;; pinky-mode to the rescue! pinky-mode is a minor mode which lets you
;; use most common navigation commands without touching [Ctrl]. It is
;; inspired by command mode in Vim. Once activated, just use the letter
;; keys to navigate your buffer. For example, press n instead of C-n to
;; move to the next line. This lets you keep using your muscle memory!
;; See the full list of key bindings below. Press i to leave Pinky
;; mode and go back to inserting text.
;;
;; When pinky-mode is active, the cursor color will change, and
;; "Pinky" will be shown in the mode line. You can customize the cursor
;; color with M-x customize-group RET pinky RET.
;;
;; pinky-mode works with window-number to switch between windows in
;; the same frame. Just tap a number key to go to any window.

;;; Installation:
;;
;; Once the pinky-mode package is installed, add this to your .emacs:
;;
;;   (require 'pinky-mode)
;;
;; You will need to bind pinky-mode-activate to something you can
;; press easily. I use the key-chord package to bind this to "jk"
;; (press j and k at the same time). This is entirely up to you
;; though.
;;
;;   (require 'key-chord)
;;   (key-chord-mode 1)
;;   (key-chord-define-global "jk" 'pinky-mode-activate)
;;
;; To force yourself to learn pinky-mode, consider disabling the
;; normal key bindings for navigation.
;;
;;   (pinky-unbind-normal-keys)

;;; Code:

(require 'window-number)

(defgroup pinky nil "Pinky mode group"
  :group 'editing)

(defcustom pinky-active-color "red"
  "The color of the point when pinky mode is active."
  :type 'color
  :group 'pinky)

;; TODO: it would be good to set the default to the normal cursor face
;; background color. I can't figure out the symbol of this though.
(defcustom pinky-inactive-color "black"
  "The color of the point when pinky mode is inactive."
  :type 'color
  :group 'pinky)

(defvar pinky-mode-in-minibuffer nil)

(defun pinky-mode-activate ()
  (interactive)
  (pinky-mode t)
  (set-cursor-color pinky-active-color)
  (blink-cursor-mode -1))

(defun pinky-mode-deactivate ()
  (interactive)
  (pinky-mode -1)
  (set-cursor-color pinky-inactive-color)
  (blink-cursor-mode t))

(defvar pinky-mode-map
  (let ((map (make-keymap)))


    ;; TODO: sexp things | back/forw-sentence
    
    ;; DEACTIVATE KEY
    (define-key map (kbd "C-h") 'pinky-mode-deactivate)

    ;; Code folding based on level
    ;; Notice the numbers are derived from char-widths, not n-of-tabs
    (define-key map "0" '(lambda () (interactive) (set-selective-display nil)))
    (define-key map "1" '(lambda () (interactive) (set-selective-display 4)))
    (define-key map "2" '(lambda () (interactive) (set-selective-display 8)))
    (define-key map "3" '(lambda () (interactive) (set-selective-display 12)))
    (define-key map "4" '(lambda () (interactive) (set-selective-display 16)))
    (define-key map "5" '(lambda () (interactive) (set-selective-display 20)))
    (define-key map "6" '(lambda () (interactive) (set-selective-display 24)))
    
    ;; undo
    (define-key map "/" 'undo)

    ;; buffer flipping
    (define-key map "b" 'other-window)
    
    ;; arrow-key layout
    (define-key map "a" 'backward-word) ; W
    (define-key map (kbd "C-a") 'backward-char)
    (define-key map (kbd "M-a") 'backward-sexp)
    (define-key map (kbd "M-C-a") 'backward-sentence)
    (define-key map "e" 'forward-word)  ; A
    (define-key map (kbd "C-e") 'forward-char)
    (define-key map (kbd "M-e") 'forward-sexp)
    (define-key map (kbd "M-C-e") 'forward-sentence)
    (define-key map "," 'previous-line) ; S
    (define-key map (kbd "C-,") 'backward-paragraph)
    (define-key map "o" 'next-line)     ; D
    (define-key map (kbd "C-o") 'forward-paragraph)

    ;; jump to line ends
    (define-key map "'" 'smarter-move-beginning-of-line) ; @unsafe from init.el
    (define-key map "." 'move-end-of-line)

    ;; scrolling (smaller amount than C-v)
    (define-key map "r" '(lambda ()
                           (interactive)
                           (scroll-up-command 5))) ; Goes down
    (define-key map "c" '(lambda ()
                           (interactive)
                           (scroll-down-command 5))) ; Goes up

    
    ;; Recenter to cursor
    (define-key map "g"   'recenter-top-bottom)
    (define-key map "C-g" 'recenter-top-bottom) ; Alias

    ;; region selection
    ;;   That used to be on [space], but when I'm using this mode, I very often
    ;;   want to realign something and I need space for that. Feels a bit
    ;;   weird to have it different from the global map though. I'll get
    ;;   used to it I'm sure.
    (define-key map "h" 'set-mark-command)

    ;; Deletion
    (define-key map "t" 'delete-backward-char)      ; backspace
    (define-key map "C-t" 'my-backward-delete-word) ; backspace word
    (define-key map "n" 'delete-char)               ; delete
    (define-key map "C-n" 'my-delete-word)          ; delete word
    
    ;; Indenting
    (define-key map "i" '(lambda () (interactive)
                           ;; Run a macro recording of hitting tab
                           (fset 'tab
                                 (kmacro-lambda-form [tab] 0 "%d"))
                           (tab)
                           ))

    ;; Copy-pasting and cutting
    (define-key map "z" 'kill-line)      ; cut line
    (define-key map "v" 'kill-region)    ; cut
    (define-key map "w" 'kill-ring-save) ; copy
    (define-key map "m" 'yank)           ; paste
    
    map))


(defun pinky-unbind-normal-keys ()
  "Unbinds the normal navigation keys to force you to save your pinkies!"
  (interactive)
  (global-unset-key (kbd "C-n"))
  (global-unset-key (kbd "C-p"))
  (global-unset-key (kbd "C-f"))
  (global-unset-key (kbd "C-b"))
  (global-unset-key (kbd "C-a"))
  (global-unset-key (kbd "C-e"))
  (global-unset-key (kbd "C-v"))
  (global-unset-key (kbd "M-v")))


(define-minor-mode pinky-mode
  "Navigate without holding meta keys"
  :lighter " Pinky" :global t :keymap pinky-mode-map)


(add-hook 'minibuffer-setup-hook (lambda nil
  (setq pinky-mode-in-minibuffer pinky-mode)
  (if pinky-mode (pinky-mode-deactivate))))

(add-hook 'minibuffer-exit-hook (lambda nil
  (if pinky-mode-in-minibuffer (pinky-mode-activate))))


(provide 'pinky-mode)

;;; pinky-mode.el ends here

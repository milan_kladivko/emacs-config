
import sys
import re

line_number  = int(sys.argv[2])    # arg1 is "Line" that emacs spews out
file_to_read = sys.argv[3]

f = open(file_to_read, "r")
all_lines = f.readlines()

current_line = all_lines[line_number - 1]

re_tabindent = re.compile(r"^([\t]*)")
def get_indent(line):
    matched_tabs = re_tabindent.match(line).group(1)
    indent = len(matched_tabs)
    return indent

current_indent = get_indent(current_line)
scopes = [current_line, ]

for line_n in reversed(range(0, line_number)):
    line = all_lines[line_n].rstrip()
    if len(line.strip()) == 0:
        continue # Don't match whitespace
    
    indent = get_indent(line)
    
    if indent < current_indent:
        current_indent = indent
        scopes.append(line)


# Print the scopes in the desired format for Emacs to split into lines
for line in reversed(scopes):
    print(line)

;;=============================================================================
;; User info
;;=============================================================================
(setq user-full-name "Milan Kladivko"
      user-mail-address "kladivko.dev@gmail.com")

;;=============================================================================
;; Startup + Improvements
;;=============================================================================

;; Stop and print error (when something throws and we don't know what)
;; (setq debug-on-error t)

(setq-default load-prefer-newer t) ;; @test

;; Display how fast emacs loaded when starting
(add-hook
 'emacs-startup-hook
 (lambda ()
   (message "Emacs ready in %s with %d garbage collections."
            (format "%.2f secs"
                    (float-time
                     (time-subtract after-init-time before-init-time)))
            gcs-done)))

;; DURING STARTUP :: Set it high, we'd like to never have to GC while starting up.
(setq gc-cons-threshold (* 512 1000 1000)
      gc-cons-percentage 0.7)


;; AT RUNTIME :: Back to default emacs values -- that's what most packages
;;               should assume and thus be optimized for it.
(add-hook 'emacs-startup-hook
          (lambda () (setq
                      ;; gc-cons-threshold (* 800 1000)
                      ;; actually, do more for LSP
                      ;; https://emacs-lsp.github.io/lsp-mode/page/performance/
                      gc-cons-threshold (* 100 1000 1000) 
                      gc-cons-percentage 0.2)
            ))

;; emacs default is too low 4k considering that the some of the LSP
;; responses are in 800k - 3M range.
(setq read-process-output-max (* 1024 1024)) ;; 1mb


;; MANUAL CONTROL OVER GC AT RUNTIME
;;   Sometime in the future, we can investigate when we need to run
;;   garbage collection specifically.
;;   We can use `garbage-collect` to force it or set it really high
;;   at key moments that we figure out by printing messages from GC
;;   by setting the `garbage-collection-messages` variable.

;; Running a garbage collection whenever it makes sense to do so
;; https://www.reddit.com/r/emacs/comments/bqu69o
(add-hook 'after-save-hook       #'garbage-collect)
(add-hook 'focus-out-hook        #'clean-buffer-list)
(add-hook 'focus-out-hook        #'garbage-collect)
(add-hook 'suspend-hook          #'garbage-collect)
;;(add-hook 'minibuffer-setup-hook #'garbage-collect)
;;(add-hook 'minibuffer-exit-hook  #'garbage-collect)
;;(add-hook 'kill-buffer-hook      #'clean-buffer-list)
;;(add-hook 'kill-buffer-hook      #'garbage-collect)

;; Fontification tweaks (whatever that means...)
(setq jit-lock-stealth-time 0.1
      jit-lock-defer-time 0.01
      jit-lock-chunk-size 500)

;; Emacs autosave (~) and backup (#) directory change
(setq backup-directory-alist
      ;; store all backup and autosave files in the tmp dir
      ;; @bug; sometimes it dumps it in ~/ but who cares, they're temps anyway
      `((".*" . ,"~/.emacs.d/.emacs_trash")))
(setq auto-save-file-name-transforms
      `((".*" ,"~/.emacs.d/.emacs_trash" t)))



;;  Encoding to UTF-8 always
(setq utf-translate-cjk-mode nil) ; disable CJK (Chinese/Japanese/Korean chars)
(set-language-environment 'utf-8)
(set-keyboard-coding-system 'utf-8-mac) ; For old Carbon emacs on OS X only
(setq locale-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-selection-coding-system
 ;; https://rufflewind.com/2014-07-20/pasting-unicode-in-emacs-on-windows
 (if (eq system-type 'windows-nt) 'utf-16-le 'utf-8))
(prefer-coding-system 'utf-8)


;; "package cl is deprecated"
;; https://github.com/kiwanami/emacs-epc/issues/35
(setq byte-compile-warnings '(cl-functions))

;;=============================================================================
;;  Package manager initializations
;;=============================================================================
(require 'package)

;; Additional package repositories
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)
(add-to-list 'package-archives
             '("gnu" . "http://elpa.gnu.org/packages/") t)

;; turn on the package manager
(package-initialize)
(setq package-enable-at-startup nil) ; no package autoload
;; use the default package install folder
(add-to-list 'load-path "~/.emacs.d/elpa/")

;; Source-code packages (pure code, no package info)
(add-to-list 'load-path "~/.emacs.d/raw-packages/")
(add-to-list 'custom-theme-load-path "~/.emacs.d/raw-themes")

;; Turn on `use-package`, and install if missing. It's better for keeping
;; dependencies and installing them if they're not present, and for having
;; configurations for packages somewhat readable.
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)


;; Some utilities for making updating easier -- browsing `package-list`
;; ...emacs.stackexchange.com/q/31872
(defun package-menu-find-marks ()
  "Find packages marked for action in *Packages*."
  (interactive)
  (occur "^[A-Z]"))
(defun package-menu-filter-by-status (status)
  "Filter the *Packages* buffer by status."
  (interactive
   (list (completing-read
          "Status: " '("new" "installed" "dependency" "obsolete"))))
  (package-menu-filter (concat "status:" status)))
;; ... bindings for those ...
(define-key package-menu-mode-map "s" #'package-menu-filter-by-status)
(define-key package-menu-mode-map "a" #'package-menu-find-marks)

;;=============================================================================
;;  PATH variables for Emacs
;;  (when running emacs from GUI it gets an old value, fix that)
;;=============================================================================

;; Checks what the PATH should be and adds the directories into Emacs.
;; Much better than doing any OS hacks. 
(use-package exec-path-from-shell
  :init
  (setq exec-path-from-shell-check-startup-files nil)
  (when (memq window-system '(mac ns x))
    (exec-path-from-shell-initialize))
  )

;;=============================================================================
;;  Theming, colors and fonts
;;=============================================================================

;; NOTE: The themes have to be loaded once per emacs session, otherwise some 
;;       colors are broken and not overwritten by the new theme. Just restart
;;       Emacs if you have issues.
(use-package monokai-theme
  :init
  (setq-default monokai-highlight "#131411") ;; too dim 4 me
  
  (setq-default monokai-comments "#bbef53")  ;; Greeny!
  (setq-default monokai-256-comments "#5F8700")  ;; Greeny! ;; TODO:  comment colors
  
  ;; @todo; Copy the settings from the `customize-face` command here
  ;;   because all colors should be in one place.
  ;;   Or don't, because it would be a waste of time -- I mean *why* would I
  ;;   do it, amirite?
  )
;; Other (maybe installed) themes
;;;; (load-theme 'colonoscopy t) ; Monochrome one

(load-theme
 'monokai t
 ;;'colonoscopy
 ;;'soft-stone
 ;;'adwaita
 )


(setq-default line-spacing 0)  ; line height increase, in height percentage

;; Custom fonts

;; NOTE: Setting the width by the attribute doesn't always work,
;;    try mentioning the width in the family name string.
;; NOTE: Some font-y things are set in customize, it's a bit of a mess honestly.

(defun /font-hack ()
  "Set font to Hack"
  (interactive)
  (set-face-attribute 'default nil
                      :family "Hack"
                      :height 100 :weight 'normal :width 'normal))
(defun /font-input ()
  "Set font to Input Mono"
  (interactive)
  (set-face-attribute 'default nil
                      :family "Input Mono Condensed"
                      :height 100 :weight 'ultra-bold :width 'normal)
  (face-spec-set 'bold :weight 'black))
(defun /font-input-big ()
  "Input Mono font with bigger base size (height)"
  (interactive)
  (set-face-attribute 'default nil
                      :family "Input Mono Condensed"
                      :height 110 :weight 'normal :width 'normal))

(defun /font-terminus ()
  "Terminus bitmap font"
  (interactive)
  (set-face-attribute 'default nil
                      :family "Terminus"
                      :height 90 :weight 'normal :width 'normal))

;; NOTE:  Inconsolata has issues with some older backends of Emacs renderer
;;   and as far as I remember, it's about `cairo`.
;;   So I'd rather not use it because then I have to remember
;;   to build Emacs a specific way with specific libraries. 
(defun /font-cons ()
  "Set font to Inconsolata"
  (interactive)
  (set-face-attribute 'default nil
                      :family "Inconsolata Semicondensed"
                      :height 110 :weight 'ultra-bold :width 'normal)
  (face-spec-set 'bold :weight 'black))

(defun /font-cons-big ()
  "Set font to Inconsolata"
  (interactive)
  (set-face-attribute 'default nil
                      :family "Inconsolata Semicondensed"
                      :height 130 :weight 'ultra-bold :width 'normal)
  (face-spec-set 'bold :weight 'black))

(defun /font-fant ()
  "Set font to Fantasque Sans Mono"
  (interactive)
  (set-face-attribute 'default nil
                      :family "Fantasque Sans Mono"
                      :height 105 :weight 'bold :width 'normal)
  (face-spec-set 'bold :weight 'normal))

(defun /font-ubuntu ()
  "Set font to Ubuntu"
  (interactive)
  (set-face-attribute 'default nil
                      :family "Ubuntu"
                      :height 130 :weight 'semi-bold :width 'normal))
(defun /font-lato ()
  "Set font to Lato"
  (interactive)
  (set-face-attribute 'default nil
                      :family "Lato"
                      :height 160 :weight 'ultra-bold :width 'normal))
(defun /font-serif ()
  ""
  (interactive)
  (set-face-attribute 'default nil
                      :family "FreeSerif"
                      :height 170 :weight 'ultra-bold :width 'normal))


(defun /font-pixel ()
  "Set font to a bitmap pixel font"
  (interactive)
  (set-face-attribute 'default nil
                      :family "November"
                      :height 120 :weight 'normal :width 'normal)
  (face-spec-set 'bold :weight 'black))
   

;; /////  SET DEFAULT FONT /////
(/font-input)
;; /////////////////////////////


;; Font for unicode chars (such as →„Σ"←)
(set-fontset-font
 t 'unicode
 (font-spec
  :family "Dejavu Sans Mono"
  :height 108 :weight 'normal	:width  'normal) nil 'prepend)

;; NOTE:  There are some options set with `customize-face` -- check the bottom
;;   of this file for what I edited.
;;   These changes should always override the current theme, I think. 


;;=============================================================================
;; Editor config
;;=============================================================================

;; basics
(setq inhibit-startup-screen t)  ; don't show the intro screen
(tool-bar-mode 0)    ; hide the fugly window toolbar
(menu-bar-mode 0)    ; hide the `File | Edit | Options ...` too
(scroll-bar-mode -1) ; hide scrollbar

(show-paren-mode 1)     ; highlight matching parentheses
(global-hl-line-mode t) ; highlight the line I'm currently on
(column-number-mode t)  ; modeline shows line:column numbers

(save-place-mode t)  ; position in closed files persist
(recentf-mode t)     ; keep a db of recently-open files


;; disable some warnings
(progn 
  (put 'narrow-to-region 'disabled nil)
  (put 'narrow-to-page 'disabled nil)
  (put 'upcase-region 'disabled nil)
  (put 'downcase-region 'disabled nil)
  (put 'erase-buffer 'disabled nil)
  (put 'scroll-left 'disabled nil)
  (put 'dired-find-alternate-file 'disabled nil)
)


;; window title based on the project and file-name
(setq
 frame-title-format
 '("emacs" (:eval
            (let ((project-name (projectile-project-name)))
              (unless (string= "-" project-name)
                (format " | %s" project-name))))))


;; echo line
;; @todo; How does this affect `doom-modeline-mode`
(size-indication-mode 0)  ; hide number of bytes in file
(setq uniquify-buffer-name-style 'post-forward)  ; same filename buffers

;; cursor blinking
(setq blink-cursor-delay 0.3)
(setq blink-cursor-interval 0.2)
(setq blink-cursor-blinks 50)
;; nah dude, it looks pretty bad ;; (blink-cursor-mode t)
(blink-cursor-mode -1)

;; indentation
(setq-default tab-width 4)           ; default tab width
(setq-default indent-tabs-mode nil)  ; but always use spaces (Go does gofmt->tabs)
(defun /ind () (interactive) 
       (setq-default
        ;; Go has tabs as indenting, very smart placement too, no work to be done
        python-indent-offset          4

        ;; Basic C -- could be used by -some- modes btw
        c-basic-offset                4

        ;; JS/TS
        web-mode-code-indent-offset   4
        js-indent-level               4
        typescript-indent-offset      4
        ;; Stupid "padding"
        web-mode-script-padding       2 ;0
        web-mode-style-padding        2 ;0
        web-mode-script-padding       2 ;0
        web-mode-part-padding         2 ;0
        web-mode-block-padding        2 ;0
        ;; CSS
        css-indent-offset             2
        web-mode-css-indent-offset    2 ; still does 4 anyway, fuck me dude
        ;; HTML
        sgml-basic-offset             2
        web-mode-markup-indent-offset 2
        web-mode-attr-indent-offset   2 ; This one doesn't work for some reason
        )
       (message "all indents set"))
(/ind)


;; leave the cursor in its place when scrolling 
;; (setq scroll-preserve-screen-position 1)


;; Mouse scroll less jumpy + ShiftTurbo
(setq mouse-wheel-scroll-amount '(2 ((shift) . 10))) ; amounts
(setq mouse-wheel-progressive-speed nil) ; no acceleration
(setq mouse-wheel-follow-mouse 't) ; scroll window under mouse
(setq scroll-step 2) ; keyboard scroll one line at a time

;; The weird-ass emacs default "We'll recenter the pane once in a while
;; when you're just moving across the lines one by one"
;;   https://zhangda.wordpress.com/2009/05/21/customize-emacs-automatic-scrolling-and-stop-the-cursor-from-jumping-around-as-i-move-it/
(setq-default scroll-margin 5 ; this is cool
              scroll-conservatively 0
              scroll-up-aggressively 0.01
              scroll-down-aggressively 0.01)
 

;; (global-auto-complete-mode t)  ; @todo; use better autocomplete

;; @todo; start using smartparens, maybe


;; --------   Custom functions   -----------------------------------------------------

;; Alt+arrow line swapping
(defun /move-line-up ()
  "Move up the current line."
  (interactive)
  (transpose-lines 1)
  (forward-line -2)
  (indent-according-to-mode))
(defun /move-line-down ()
  "Move down the current line."
  (interactive)
  (forward-line 1)
  (transpose-lines 1)
  (forward-line -1)
  (indent-according-to-mode))

;; Indent-aware home / jump to beginning of line
;; emacsredux.com/blog/2013/05/22/smarter-navigation-to-the-beginning-of-a-line/
(defun /smarter-move-beginning-of-line (arg)
  "Move point back to indentation of beginning of line."
  (interactive "^p")
  (setq arg (or arg 1))
  ;; Move lines first
  (when (/= arg 1)
    (let ((line-move-visual nil))
      (forward-line (1- arg))))
  (let ((orig-point (point)))
    (back-to-indentation)
    (when (= orig-point (point))
      (move-beginning-of-line 1))))

;; Deletion by words
;;  ( without touching the clipboard/kill-ring )
;; http://ergoemacs.org/emacs/emacs_kill-ring.html
;; NOTE:  When using `cua-mode`, it is not necessary to do -- it does that
;;   by default to keep behavior the same as with the rest of the system. 
(defun /delete-word (arg)
  "Delete characters forward until encountering the end of a word.
With argument, do this that many times.
This command does not push text to `kill-ring'."
  (interactive "p")
  (delete-region
   (point)
   (progn
     (forward-word arg)
     (point)))
  )
(defun /backward-delete-word (arg)
  "Delete characters backward until encountering the beginning of a word.
With argument, do this that many times.
This command does not push text to `kill-ring'."
  (interactive "p")
  (/delete-word (- arg))
  )

(defun /kill-current-buffer ()
  (interactive)
  (kill-buffer (current-buffer))
  )

;; Hide comments in code. This hides lines that have nothing
;; but whitespace and comments.
(use-package hide-comnt)

;; When run in terminal mode,
;; uses `xclip` to use the OS clipboard. 
(use-package xclip-mode
  :init
  (xclip-mode 1)
  :defer t 
  )

;; ===========================================================
;;                     SMOOTH SCROLLING
;; ===========================================================

;; Scrolling (default's unusable, dude)
;; (global-set-key (kbd "M-v")  ; BACK
;;                 (lambda ()
;;                   (interactive)
;;                   (scroll-down-command 10)))
;; (global-set-key (kbd "C-v")  ; FORWARD
;;                 (lambda ()
;;                   (interactive)
;;                   (scroll-up-command 10)))

;; TODO:  Might want to clean this up... or not. It works well enough.

(setq /smooth--timer nil)
(defun /smooth---up() (interactive)

       ;; yes, up is down
       (scroll-down-command (pop /smooth--list))
       (setq /smooth--remaining (+ /smooth--remaining -1))

       (when (>  /smooth--remaining 0)
         (setq /smooth--timer (run-with-timer 0.016 nil '/smooth---up))) ;; Recurse!
       (when (<= /smooth--remaining 0)
         (setq /smooth--timer nil)) ;; Do nothing and set a nil timer
       )
(defun /smooth-up () (interactive)
       ;; Cleanup and setup
       (when /smooth--timer (cancel-timer /smooth--timer)) 
       (setq /smooth--remaining 7
             /smooth--list (list 3 2 2 1 1 1 1))
       ;; Dispatch recursive calls
       (/smooth---up))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun /smooth---down() (interactive)

       ;; yes, up is down
       (scroll-up-command (pop /smooth--list))
       (setq /smooth--remaining (+ /smooth--remaining -1))

       (when (>  /smooth--remaining 0)
         (setq /smooth--timer (run-with-timer 0.016 nil '/smooth---down))) ;; Recurse!
       (when (<= /smooth--remaining 0)
         (setq /smooth--timer nil)) ;; Do nothing and set a nil timer
       )
(defun /smooth-down () (interactive)
       ;; Cleanup and setup
       (when /smooth--timer (cancel-timer /smooth--timer)) 
       (setq /smooth--remaining 7
             /smooth--list (list 3 2 2 1 1 1 1))
       ;; Dispatch recursive calls
       (/smooth---down))

;;=============================================================================
;;  Navigation and projects
;;=============================================================================

;; Display help for uncompleted shortcut sequences.
;; Note:  `discover` and `which-key` both do a similiar thing. I hope they
;;   don't clash when using both... I don't even know if I *need* both.
(use-package discover
  :init (global-discover-mode 1))
(use-package which-key
  :config (which-key-mode))

;; Listing recently visited files
(use-package recentf)
(setq
 ;; ...Sometimes it includes some background stuff that I don't care about. 
 recentf-exclude '(".emacs.d/session" "ido.last"
                   ".*_flymake.*" ".ftp:.*" ".sudo:.*"
                   ".emacs.d/tmp/*")
 recentf-keep '(file-remote-p file-readable-p)
 recentf-max-saved-items 200  ; this was set to 500, and things were slow
 )

;; Using `ido` to browse `recentf` stuff
;; https://gist.github.com/rmcm/488405
;; TODO:  Bind to the recentf map on spacebar or something
(defun /recentf-ido-find-file ()
  "Find a recent file using Ido."
  (interactive)
  (let ((file (ido-completing-read
               "Choose recent file: "
               recentf-list nil t)))
    (when file
      (find-file file))))
(defun /recentf-ido-find-dired ()
  "Find a recent directory using Ido."
  (interactive)
  (let ((dir (ido-completing-read
              "Choose recent directory: "
              (delete-dups (mapcar  'file-name-directory recentf-list)) nil t)))
    (when dir
      (find-file dir))))


(use-package doom-modeline
  ;; A prettier modeline
  ;;  (...the thing at the bottom that shows current file etc...)
  :ensure t
  :after (projectile)
  :hook (after-init . doom-modeline-mode)
  :config
  (setq doom-modeline-project-detection 'projectile) 
  (setq doom-modeline-buffer-file-name-style
        'truncate-with-project)  ; hide folder hierarchy to project
  (setq doom-modeline-buffer-encoding nil)  ; hide encoding
  (setq doom-modeline-major-mode-icon nil)  ; hide weird mode icon
  (setq doom-modeline-icon nil)  ; don't use those ugly icons, man
  ;; @todo; Disable git branch display...
  (setq doom-modeline-height 25))


(use-package terminal-here
  ;; Open a terminal in the current directory
  :init

  ;; Set the terminal command that we want to use.
  (if (eq system-type 'windows-nt)
      (setq terminal-here-terminal-command '("cmder")))
  (if (eq system-type 'gnu/linux)
      (setq terminal-here-terminal-command '("kitty")))
  
  :config
  (defun /term () (interactive) (terminal-here-launch)) ; Alias command
  )


;; Magit - A useable git client.
(use-package magit
  :bind ("C-x g" . magit-status)

  ;; Delete ALL magit buffers and helpers, don't leave anything
  ;; behind.
  ;; from: https://wisdomandwonder.com/article/10803/
  :config
  (defun help/magit-kill-buffers ()
    "Restore window configuration and kill all Magit buffers"
    (interactive)
    (let ((buffers (magit-mode-get-buffers)))
      (magit-restore-window-configuration)
      (mapc #'kill-buffer buffers)))
  (bind-key "q" #'help/magit-kill-buffers magit-status-mode-map))


;; Projectile - Managing projects.
;; Simplest thing to do is to make a git repo and open that repo.
;; The package will just interpret that folder as a project folder
;; and we're good to go.
(use-package projectile
  :config
  :bind-keymap
  ("<f9>" . projectile-command-map)
  ("C-a"  . projectile-command-map)
  ("C-."  . projectile-command-map)
  
  :init
  (if (eq system-type 'windows-nt)
      (setq projectile-project-search-path
            '("c:/Users/milan.kladivko/Desktop/work")))
  (if (eq system-type 'gnu/linux)
      (setq projectile-project-search-path
            '("~/work")))
  (projectile-mode 1))

;; Sidebar directory view
(use-package neotree)


;; Ido mode - file quick-search
(use-package ido
  :config
  (setq ido-enable-flex-matching t) ; fuzzy-match 
  (setq ido-ignore-files '("#$" "~$")) ; emacs garbage 
  ;; show choices vertically
  (setf (nth 2 ido-decorations) "\n")
  
  (ido-everywhere t)
  (ido-mode 1)
  ;; stop ido from suggesting when naming new file
  (when (boundp 'ido-minor-mode-map-entry)
    (define-key (cdr ido-minor-mode-map-entry) [remap write-file] nil))
  )


;; Buffer browser that doesn't suck ass.
;; Basically, it lets me group things with a regexp so that I can
;; glance-choose a .go file for example. Pretty useful. Sometimes.
(use-package ibuffer
  :config
  (setq ibuffer-show-empty-filter-groups nil) ; Hide empty groups
  (add-hook 'ibuffer-mode-hook '/trunc)  ; truncate long filepaths
  
  ;; Buffer Grouping to make the buffer more glanceable
  ;; TODO:  by projects :: https://github.com/purcell/ibuffer-projectile
  (add-hook 'ibuffer-mode-hook
            '(lambda ()
               (ibuffer-switch-to-saved-filter-groups 
                "default-groups")
               (hardbreak)))
  (setq ibuffer-saved-filter-groups
        '(("default-groups"
           ;; NOTE: ordering matters!

           ;; Special emacs buffers (not meant to be interacted with)
           ("......" (or (name . "\\*$")
                         (name . "^magit-process")
                         (name . "^magit-diff")))
           
           ("---- NOTES ----" (or (mode . org-mode)
                        (mode . markdown-mode)))
           
           
           
           ;; Languages   (here, I'd like to rather see a sort by filename)
           ("---- GO ----" (filename . "\\.go$"))
           
           ("---- Web | CSS ----" (filename . "\\.s?css$"))
           ("---- Web | HTML ----" (filename . "\\.html$"))
           ("---- Web | JS ----" (filename . "\\.js$"))
           ("---- Web | TS ----" (filename . "\\.ts$"))
           ("---- Web | Svelte ----" (filename . "\\.svelte$"))
           
           ("---- Python ----" (filename . "\\.py$"))
           ("---- Java ----" (filename . "\\.java$"))
           
           
           ("---- Text | Markdown ----" (filename . "\\.md$"))
           ("---- Text | TXT ----" (filename . "\\.txt$"))

           ("---- MAGIT ----" (name . "^magit: "))
           
           ("---- SEARCH ----" (mode . ag-mode))
           ("---- DIR ----" (mode . dired-mode))
           
           ("---- Emacs ----" (filename . "emacs"))
           
           )))
  )

;;=============================================================================
;; Editor part of emacs
;;=============================================================================

;; Swapping panels (unnecessary)
(load "buffer-move")
(use-package buffer-move
  :bind
  ("<s-up>" . buf-move-up)
  ("<s-down>" . buf-move-down)
  ("<s-left>" . buf-move-left)
  ("<s-right>" . buf-move-right))

;; Fullscreen single-file view
(load "centered-window")
(defun /read () (interactive)
       (setq cwm-centered-window-width 100) ; Based on default charwidth
       (if (centered-window-mode-toggle) ; Toggles and returns true if enabled
           (progn
             (delete-other-windows)  ; Show only the one window
             (text-scale-adjust 0)   ; Reset scale
             (text-scale-adjust +1)  ; One step bigger from default
             (message "centering ON")
             )
         (progn
           (text-scale-adjust 0)     ; Leave disabled and restore font size
           (message "centering OFF")
           )
         )
       )
;; Centered single-file view
(defun /center () (interactive)
       ;; In default character size units
       ;; Also note that we should be able to go up one font size and still be able
       ;; to fit 90 characters into the pane.
       (setq cwm-centered-window-width 90)
       (if (centered-window-mode-toggle)
           (delete-other-windows)))
(defun /center-thin () (interactive)
       (setq cwm-centered-window-width 50)
       (if (centered-window-mode-toggle)
           (delete-other-windows)))

;; Long line breaks
(defun /trunc ()
  (interactive)
  (toggle-truncate-lines +1)
  (message "Lines cut off at window end"))
(defun /wordwrap ()
  (interactive)
  (toggle-truncate-lines 0)
  (toggle-word-wrap +1)
  (message "Word break"))
(defun /hardbreak ()
  (interactive)
  (toggle-truncate-lines 0)
  (toggle-word-wrap 0)
  (message "Fold lines at window end"))


;;=============================================================================
;;  Hooks for specific languages/modes
;;=============================================================================

;;
;; `compilation` mode in emacs -- `compile` command
;; Has a builtin feature to jump to file points written as `~/dir/to/file:30`
;; You can use it for more than just compilation -- it's also useful for
;; doing searches or other file-listing commands. 
;;

;; A small tweak to allow color codes from commands to be parsed correctly. 
(require 'ansi-color)
(defun colorize-compilation-buffer ()
  (toggle-read-only)
  (ansi-color-apply-on-region compilation-filter-start (point))
  (toggle-read-only))
(add-hook 'compilation-filter-hook 'colorize-compilation-buffer)

;; Autocompletion engine used by LSP
(use-package company

  ;; Main gripe I have with autocompletion:
  ;;   Don't mess with my typing unless I tell you to.
  ;; With that, bring up the autocompletion with a key and use special
  ;; keys that I'm not likely to hit while editing files.
  
  :config
  ;;(setq company-idle-delay 1)   ; Wait 1s after typing stopped, then display 
  (setq company-idle-delay nil) ; Disable bringing up while typing

  :config
  ;; ...gotta disable the messy keys from the mode map (see `company.el`)
  ;;(define-key keymap (kbd "<down>") 'company-select-next-or-abort)
  ;;(define-key keymap (kbd "<up>") 'company-select-previous-or-abort)
  ;;(define-key keymap [return] 'company-complete-selection)
  ;;(define-key keymap (kbd "RET") 'company-complete-selection)
  ;;(define-key keymap [tab] 'company-complete-common)
  ;;(define-key keymap (kbd "TAB") 'company-complete-common)
  (unbind-key (kbd "<down>") company-active-map)
  (unbind-key (kbd "<up>") company-active-map)
  (unbind-key [return] company-active-map)
  (unbind-key (kbd "RET") company-active-map)
  (unbind-key [tab] company-active-map)
  (unbind-key (kbd "TAB") company-active-map)
  ;; ;; ...and then rebind correctly
  :bind (:map company-active-map
              ("C-<down>"   . company-select-next)     ; no abort
              ("C-<up>"     . company-select-previous) ; no abort
              ("C-t"        . company-complete-selection) ; complete the highlight
              ("C-<return>" . company-complete-selection) ; complete the highlight
              )
  )
(use-package company-capf)

;; LSP - Language Server Protocol
;; https://github.com/golang/tools/blob/master/gopls/doc/emacs.md
(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :hook ((go-mode . lsp-deferred)                       ; Golang
         (web-mode . lsp-deferred)                      ; Svelte, HTML, web
         (typescript-mode . lsp-deferred)               ; TS, web
         ; TODO:  Typescript mode doesn't use svelte LSP but should.
         (lsp-mode . lsp-enable-which-key-integration)  ; Which-key (discovery)
         )
  
  :config
  ;; NEVER ENABLE SNIPPETS
  ;; I hate them and whenever I'll need a snippet, I'll make a keeb macro. 
  (setq lsp-enable-snippet nil)
  ;; File watchers are only useful when the files change from the outside,
  ;; which -while happens- doesn't get processed well anyway for Svelte mode.
  ;; I'll just restart instead -- it's more predictable that way.
  (setq lsp-enable-file-watchers nil)
  
  ;; Global autocomplete is not needed when LSP has its own, context-aware one.
  ;; TODO:  For some reason, this is not setting the autocomplete
  ;;   correctly. It's still enabled in some cases.
  (auto-complete-mode -1)

  ;; Disable the stupid header, it varies way too much between languages
  ;; and thus I don't use it.
  (setq lsp-headerline-breadcrumb-enable nil)
  
  ;; Flycheck is the default, but we'll mention it directly as we'll configure
  ;; that one specifically.
  (setq lsp-diagnostics-provider :flycheck)
  
  ;; Custom settings
  ;; @todo;  Look at how this combines with dir-local variables
  ;;   to enable config-per-project.
  ;;(lsp-register-custom-settings
  ;; '(("gopls.completeUnimported" t t)
  ;;   ("gopls.staticcheck" t t)))  ; This doesn't work for example
  )

;; Highly customizable UI components (hover boxes, inline text) for LSP
(use-package lsp-ui :commands lsp-ui-mode)

;; The UI has some icons so I gotta have the fonts for that, sadly...
;; NOTE: First time on a machine, `M-x all-the-icons-install-fonts`
;;   to actually download the fonts. This is just a QOL package.
(use-package all-the-icons)


;; ====


;; The basic mode itself (highlighting)
(use-package go-mode)

;; Set up before-save hooks to format buffer when using LSP.
;; Make sure you don't have other gofmt/goimports hooks enabled.
(defun lsp-go-install-save-hooks ()
  (add-hook 'before-save-hook #'lsp-format-buffer t t)
  ;;(add-hook 'before-save-hook #'lsp-organize-imports t t) ; Goimports are weird
  )
(add-hook 'go-mode-hook #'lsp-go-install-save-hooks)

;; Generate method stubs for implementing an interface
(use-package go-impl)


;; ====


;; Using Web Mode for various web-based things (they usually need to parse HTML)
;; TODO:  Consider `multi-web-mode` for this -- we don't actually use any
;;   svelte-specifict so we can just use whatever there is. The `web-mode` svelte
;;   support is pretty garbage anyway -- I don't even think I use it!
;; NOTE:  SCSS is broken and if we edit the SCSS in .svelte files, it outright
;;   stops doing syntax highlighting for the entire file.
;; TODO:  Stop using `web-mode`
(use-package web-mode
  :mode "\\.php\\'"
  :mode "\\.html\\'"
  :mode "\\.svelte\\'"
  
  ;; (add-hook 'web-mode-hook 'emmet-mode)
  
  :config
  (setq-default web-mode-enable-auto-indentation  nil
                web-mode-enable-auto-quoting      nil)

  ;; Svelte
  ;;   The template parens get applied even though it's in <script> which
  ;;   breaks not only colors but sometimes even indentation which is really fucky.
  ;;
  ;;   https://github.com/fxbois/web-mode/issues/1091
  ;;     .. the maintainer just shrugged it off -- ye sure you can have templates
  ;;        in script, in comments, sure. Styling gets templates too. What a dipshit.

  (setq-default
   web-mode-engine-open-delimiter-regexps
   (list
    '("svelte"           . "oh web mode, you can't even svelte {.")

    ;; And then copy over the rest of the list -- I don't even know how
    ;; to modify arrays in elisp
    '("angular"          . "{{")
    '("asp"              . "<%\\|</?[[:alpha:]]+:[[:alpha:]]+\\|</?[[:alpha:]]+Template")
    '("aspx"             . "<%.")
    '("django"           . "{[#{%]\\|^#")
    '("elixir"           . "<%")
    '("go"               . "{{.")
    '("php"              . "<\\?")
    '("python"           . "<\\?")
    '("vue"              . "{{\\|[:@][-[:alpha:]]+=\"")
    )) 
  )


(setq ag-arguments
      '("--smart-case" "--stats"
        ;; Ignore stupid frontend build files
        "--ignore" "bundle.js"
        "--ignore" "bundle.css"
        "--ignore" "bundle.js.map"
        "--ignore" "bundle.css.map"
        "--ignore-dir" "node-modules"
        ))

;; ====


;; Listing parent levels of indents based on where our cursor is in the file.
;; @todo; Rewrite in Go, what is this python garbage dude
(defun /gs ()
  (interactive)
  (setq /scopes-file "/home/mikl/.emacs.d/raw-packages/scopes.py")
  (setq /py-output (shell-command-to-string
                    (format "python3 %s %s %s"
                            /scopes-file (what-line) (buffer-file-name))))
  (/create-popup-wintop /py-output)
  )
(defun /gsc () (interactive) (popup-hide /popup))
(defun /create-popup-wintop (str)
  (setq /popup (popup-create (window-start) 80 10))
  (popup-set-list /popup (split-string str "\n"))
  (popup-draw /popup)
  )



;; Markdown
(use-package markdown-mode
  :init
  (defun markdown-enable-section-reordering ()
    "Enable reordering sections with Alt-arrow'ing headings, 
or it just swaps the lines if line isn't a heading."
    (interactive)
    
    (defun markdown-move-heading (down)
      (interactive "nMove heading down by: ")
      (let* ((cur-line (buffer-substring-no-properties
                        (line-beginning-position)
                        (line-end-position)))
             (match (string-match-p "^#" cur-line)))
        (cond ((numberp match)
               (if (< 0 down)
                   (markdown-move-down)
                 (markdown-move-up)))
              (t
               (if (< 0 down)
                   (move-line-down)
                 (move-line-up))))))
    
    (local-set-key (kbd "M-<up>")
                   (lambda() (interactive) (markdown-move-heading -1)))
    (local-set-key (kbd "M-<down>")
                   (lambda() (interactive) (markdown-move-heading +1))))
  (add-hook 'markdown-mode-hook 'markdown-enable-section-reordering)
  (add-hook 'markdown-mode-hook '/wordwrap))




;;=============================================================================
;;  Keybindings
;;=============================================================================

;; NOTE:  Keybinds are better placed at the end -- some "maps" that we bind
;;   our keys to might throw an error because they don't exist yet. 

;; NOTE:  Since we're using a PROGRAMMABLE KEYBOARD,
;;   I'll get rid of most default emacs behavior and keystrokes.
;;   It is beneficial to make everything less Alt-based, replace nonsensical
;;   defaults and overwrite what we can and what makes sense. 

;; Use the usual cut/copy/paste keys. Not the whacky Emacs ones. 
;;
;; It would be confusing to be using anything else in the rest of my system.
;; Again, programmable keyboard, so I'm likely to have these as shortcuts.
;;
;; Takes control of:
;; - C-c  (but still allows C-c as a prefix)
;; - C-x  (but still allows C-x as a prefix)
;; - C-v
;; Thus, we can use (but they are far away, so why would I...)
;; - C-w
;; - C-y
;;
;; There are also other text-editing changes that fall in line with
;; modern workflows instead of Emacs strangeness: 
;; - Changes the C-Backspace and C-Delete word-deletions to not interact
;;   with the kill-ring (clipboard).
;; - When a selection is made and the user writes, the selection is replaced
;;   by the input. Emacs instead used to cancel the selection and write
;;   to wherever the caret was when selecting. 
(cua-mode t)
(setq cua-keep-region-after-copy t) ; Otherwise we'd lose the selection -- I'd rather ESC
(global-unset-key (kbd "C-w")) ; Just don't cut with that ever. I'd be confused a lot. 

;; OFF-LIMITS KEYS:
;;  - `C-i` is always Tab       -- handled in Emacs itself.
;;  - `C-m` is always Enter     -- handled in Emacs itself.
;;  - `C-h` is always Backspace -- handled by terminals.
;;  - Leave `C-a` empty for projectile prefix. If a selection over the whole
;;    file is needed, use C-PgUp, Shift, C-PgDown.

(bind-keys*
 
 ("<f5>"    . revert-buffer) ; just a `g` or `C-g` in some modes though

 ;; In Emacs, there is no `redo`. There is only the amazing undo system
 ;; that is only appended to and never destroyed.
 ;; You must "undo the undo" as it may. 
 ("C-z" . undo)
 
 ;; File browsing
 ("C-o"   . ido-find-file)      ; fuzzy-finder for files
 ;; NOTE: Use `C-f` when in ido to trigger the vanilla finder
 ("C-S-o" . recentf-open-files) ; list recently opened files
 
 ("C-b"   . neotree)            ; file sidebar relative to opened file
 ("C-S-b" . neotree-dir)        ; TODO: not sure what this one does

 ("C-w" . /kill-current-buffer) ; (get it back with a quick C-S-o ENT)
 ("C-S-w" . ido-kill-buffer) ; Close some buffer (quick enter closes current)

 ;; TODO:  Maybe we'd like to rebind some of the projectile things!?
  
 ;; Browse open buffers
 ("C-b" . ido-switch-buffer) ; fuzzy-finder for open buffers
 ("C-d" . ido-switch-buffer) 
 ("C-S-d" . ibuffer)         ; list open buffers, categorized
 
 ;; Command by name
 ("M-x"   . execute-extended-command) ; default
 ("C-p"   . smex)                     ; with suggestions, same key as vscode
 ("C-S-p" . execute-extended-command) ; if you needed the vanilla one
 
 ;; Eval region for when we're testing the emacs config
 ("C-q" . eval-region)
 )

(bind-keys*
 :map global-map ;; This prevents these running in the echo-line and other collisions
 ("C-s" . save-buffer)
 
 ("C-h" . query-replace)
 ;; NOTE:  There's a lot of options for this one.
 ;; TODO:  We might want to find a better one for doing vscode-style regexps
 ;;   in replacing. The Emacs one is full of nasty escapes that are really dumb.
 )

;; I never use the escape key as a Meta!
;; Make the ESC key behave exactly as the C-g key that cancels everything.
;; My programmable keyboard has an ESC key pretty so I'd prefer it. 
(define-key key-translation-map (kbd "ESC") (kbd "C-g")) ; alias key


(bind-keys* ; I-search -- with some changes
 :map global-map ;; This prevents these running in the echo-line and other collisions
 
 ;; Search 
 ("C-f" . isearch-forward) ; Incremental find, only forward
 ;; NOTE:  `C-f C-r` to search back ward 

 ;; Extending the search's sub-bindings... 
 :map isearch-mode-map
 ;; Jumping between the search results
 ;; (if the search is empty, it retrieves from history too)
 ("<up>"   . isearch-repeat-backward)
 ("<down>" . isearch-repeat-forward)
 ;; Jumping through the history
 ("C-<up>" . isearch-ring-retreat)
 ("C-<up>" . isearch-ring-advance)
 ;; I-Search finisher -- go to beginning/end of match and exit isearch
 ("<left>" . (lambda () (interactive)
               (isearch-exit)
               (when (and isearch-other-end
                          isearch-forward)
                 (goto-char isearch-other-end))))
 ("<right>" . (lambda () (interactive)
                (isearch-exit)
                (when (and isearch-other-end
                           (not isearch-forward))
                  (goto-char isearch-other-end))))
 )

(bind-keys* ; Splitpanes / windows
 :prefix "C-S-e" :prefix-map /map-window-manipulations 
 ;; Basic window manipulation -- splitpane and such 
 ("n" . delete-other-window) ; TODO:  Doesn't work and I don't know why
 ("e" . delete-window)
 ("i" . split-window-right)
 ("." . split-window-below)
 )

(bind-keys* ; Coding

 ;; That's often enough that I want it globally
 ("C-r" . lsp-rename)
 ("C-S-f" . ag-project)
 ("C-t" . company-capf)
 
 :prefix "C-n" :prefix-map /map-coding-operations

 ;; -- General -- 

 ("k"   . hide/show-comments-toggle) ; Toggle visibility of [k]omments
 ("l" . occur)                       ; [l]ist occurrences in buffer
 ("o" . projectile-find-file)        ; fuzzy search to [o]pen a file in project
 ("f" . ag-project)                  ; [f]ind all occurrences in the whole project
 ("i" . magit-status)                ; mag[i]t
 
 ("a" . align-regexp)                ; [a]lign to columns based on regexp
 ;; TODO:  Come up with a general regexp to use -- there's probably an option for
 ;;   most cases. 

 ;; -- General / LSP --

 ("C-SPC" . company-capf) ; autocomplete used by lsp
 ;; ("C-<return>" . company-capf) ; doesn't work -- there's rect functions in there!! 
 ;; ...see binds for the mode itself in map

 ("r" . lsp-rename)                ; [r]ename symbol occurrences
 ("d" . lsp-find-definition)       ; jump to [d]efinition 
 ("u" . lsp-find-references)       ; list [u]sages
 ("s" . lsp-find-workspace-symbol) ; search for [s]ymbol
 ("e" . lsp-ui-flycheck-list)      ; list [e]rrors
 
 ("w" . lsp-ui-sideline-apply-code-actions) ; do suggested actions for [w]arnings

 ;; -- GO --
 
 ;; Generate method stubs for implementing an [I]nterface
 ("C-S-i" . go-impl)
 ;; Gofmt on the buffer / region
 ("m"     . gofmt) ; not needed, go does fmt on save
 ;; Godoc -- documentation of thing
 ("S-d"   . godoc) ; TODO:  Godoc at point (I don't want to write it all the time)
 )



(bind-keys* ; Text navigation
 
 ;; Navigating to the other window
 ("C-<tab>" . other-window)
 
 ;; Jumping to places in any of the open windows.
 ("C-t"   . ace-jump-mode)      ; only for word-starts and symbols (99% of usages)
 ("C-S-t" . ace-jump-line-mode) ; also very useful
 ;; TODO:  but it's also kind of trash, I'd rather jump by line numbers or something?
 ;;   Thinking of Vim line numbers here... 
 
 ("C-l" . goto-line)

 ;; NOTE:  It's Emacs, use I-search a lot! 
 
 ;; Indent-aware "beginning of line"
 ("<home>" . /smarter-move-beginning-of-line)

 ;; Swapping lines
 ("M-<up>"   . /move-line-up)
 ("M-<down>" . /move-line-down)

 ;; "Smooth" scrolling up and down with the keyboard
 ("C-<down>" . /smooth-down)
 ("C-<up>"   . /smooth-up)
 
 ("C-e" . recenter-top-bottom) ; scroll a bit around the current line
 )

;; "mark" navigation

;;  DOCS for the mark pop and move function
;;(set-mark-command ARG)
;;
;;Set the mark where point is, and activate it; or jump to the mark.
;;Setting the mark also alters the region, which is the text
;;between point and mark; this is the closest equivalent in
;;Emacs to what some editors call the "selection".
;;
;;With no prefix argument, set the mark at point, and push the
;;old mark position on local mark ring.  Also push the new mark on
;;global mark ring, if the previous mark was set in another buffer.
;;
;;When Transient Mark Mode is off, immediately repeating this
;;command activates ‘transient-mark-mode’ temporarily.
;;
;;With prefix argument (e.g., C-u C-@), jump to the mark, and set the mark from
;;position popped off the local mark ring (this does not affect the global
;;mark ring).  Use C-x C-@ to jump to a mark popped off the global
;;mark ring (see ‘pop-global-mark’).
;; ...Because the function wants that argument, I'll just alias
;;    pressing that argument key or whatever. It has a non-overlapping
;;    binding so nothing will break. 
;;(define-key key-translation-map (kbd "M-<left>") (kbd "C-u C-@")) ; alias key
;;(define-key key-translation-map (kbd "M-<right>") (kbd "C-u C-@")) ; alias key
;; But that seems to have issues with using that second part with the symbol...

(bind-keys* ; some are just functions to call without arguments...
 ("M-<left>" . (lambda () (interactive)
                 (set-mark-command 4) ;... 4 should be the C-u without further arguments
                 ))
 ;; TODO:  How do I go forward again though?? This ring stuff is mind-bending
 ("M-S-<left>" . pop-global-mark)
 )


;;=============================================================================
;; Open file on Emacs startup
;;=============================================================================

;; open the todo list 
;;(find-file "~/todo.md")


;;=============================================================================
;; Viewing log files with colored output
;;=============================================================================
(require 'ansi-color)
(defun /ansicol ()
  (interactive)
  (ansi-color-apply-on-region (point-min) (point-max)))

;;=============================================================================
;; Some old things I had in Customize...
;;'(default ((t (:inherit nil :background "#EDEDED" :foreground "#005F5F"))))
;;'(font-lock-builtin-face ((t (:foreground "#F92672" :weight ultra-bold))))
;;'(font-lock-keyword-face ((t (:foreground "#F92672" :weight ultra-bold))))
;;=============================================================================
;;
;; Variables set using the 'customize' command with GUI
;;
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#3c3836" "#fb4933" "#b8bb26" "#fabd2f" "#83a598" "#d3869b" "#8ec07c" "#ebdbb2"])
 '(compilation-message-face 'default)
 '(custom-safe-themes
   '("413ba24c4f8a0d187a43d69dc7cbfd8b1d8782739422ba2368eb5f0893f0642a" "d9646b131c4aa37f01f909fbdd5a9099389518eb68f25277ed19ba99adeb7279" "69b47464578d342dab510f7c3267c57b4a97f5930b7ed963e6332559ef0dde75" "c8b13d8770378025d335ab0fd234cb76b80f9056edfa58b1682509cbffc24a06" "6278c6167b1f69ac38ee9d60dd3c0c659a2f7133fc3984981c0c94ac50d56a65" "c0a7c8590ec62863f8470481c749e993cddfa1d30a0c7f35c81536faaa968cf2" "5ae4d52977a13212da7ec2f6932b6449f7d7c07b3cb01085f018c0d4a7f38cec" "80a23d559a5c5343a0882664733fd2c9e039b4dbf398c70c424c8d6858b39fc5" "7c32d87e3f35d93754efca582a250e03a3a4eab13cc2f89239df9e977a47c448" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" "c74e83f8aa4c78a121b52146eadb792c9facc5b1f02c917e3dbb454fca931223" default))
 '(fci-rule-color "#3C3D37")
 '(highlight-changes-colors '("#FD5FF0" "#AE81FF"))
 '(highlight-tail-colors
   '(("#3C3D37" . 0)
     ("#679A01" . 20)
     ("#4BBEAE" . 30)
     ("#1DB4D0" . 50)
     ("#9A8F21" . 60)
     ("#A75B00" . 70)
     ("#F309DF" . 85)
     ("#3C3D37" . 100)))
 '(magit-diff-use-overlays nil)
 '(package-selected-packages
   '(markdown-toc all-the-icons go-impl xclip neotree pug-mode yaml-mode typescript-mode lsp-ui company lsp-mode which-key ace-jump-mode popup exec-path-from-shell go-complete go-mode doom-modeline elpy flycheck whitespace-cleanup-mode gitignore-mode window-number git-gutter ag js2-mode web-mode discover json-mode comint-intercept helm-gtags helm markdown-mode use-package terminal-here smart-mode-line flx-ido monokai-theme projectile magit smex s pyvenv highlight-indentation find-file-in-project auto-complete))
 '(pos-tip-background-color "#FFFACE")
 '(pos-tip-foreground-color "#272822")
 '(sml/mode-width '\"none\")
 '(vc-annotate-background nil)
 '(vc-annotate-color-map
   '((20 . "#F92672")
     (40 . "#CF4F1F")
     (60 . "#C26C0F")
     (80 . "#E6DB74")
     (100 . "#AB8C00")
     (120 . "#A18F00")
     (140 . "#989200")
     (160 . "#8E9500")
     (180 . "#A6E22E")
     (200 . "#729A1E")
     (220 . "#609C3C")
     (240 . "#4E9D5B")
     (260 . "#3C9F79")
     (280 . "#A1EFE4")
     (300 . "#299BA6")
     (320 . "#2896B5")
     (340 . "#2790C3")
     (360 . "#66D9EF")))
 '(vc-annotate-very-old-color nil)
 '(weechat-color-list
   '(unspecified "#272822" "#3C3D37" "#F70057" "#F92672" "#86C30D" "#A6E22E" "#BEB244" "#E6DB74" "#40CAE4" "#66D9EF" "#FB35EA" "#FD5FF0" "#74DBCD" "#A1EFE4" "#F8F8F2" "#F8F8F0")))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ace-jump-face-background ((t (:background "#272822" :foreground "dark slate gray" :inverse-video nil))))
 '(ace-jump-face-foreground ((t (:background "#272822" :foreground "magenta" :inverse-video nil :underline nil :weight bold))))
 '(cursor ((t (:background "red"))))
 '(font-lock-builtin-face ((t (:foreground "#F92672" :weight bold))))
 '(font-lock-comment-delimiter-face ((t (:weight normal))))
 '(font-lock-comment-face ((t (:weight normal))))
 '(font-lock-keyword-face ((t (:foreground "#F92672" :weight bold))))
 '(region ((t (:inherit highlight :extend t :background "navy"))))
 '(web-mode-comment-face ((t (:weight normal))))
 '(whitespace-line ((t (:background "gray95")))))
(put 'dired-find-alternate-file 'disabled nil)
(put 'upcase-region 'disabled nil)

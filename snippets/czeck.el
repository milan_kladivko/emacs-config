

(defun czeck ()
  "Check the file for czech diacritics and warn if present"
  (interactive)  ;; run with M-x if wanted

  (let (return-to-point
        match-point
        re-pattern-czech)
    (if (string-match "\\(.scss$\\)"
                      (buffer-file-name))
        (progn
          ;; remember position when calling func
          (setq return-to-point (point))

          ;; search for illegal chars
          (goto-char (point-min))
          (setq re-pattern-czech "[áóéíěýůúÁÓÉÍĚÝŮÚďťňščřžĎŤŇŠČŘŽ]")
          (setq match-point
                (re-search-forward re-pattern-czech
                                   (point-max) t))
          
          ;; warn if file is illegal
          (if (not (equal nil
                          match-point))
              (if (y-or-n-p "CZECH CHARS IN THIS FILE ... Wanna fix them? ")
                  
                  (progn
                    ;; found, I wanna deal with them. Highlight and move to match
                    (message "Highlighting matches ... cancel with `M-s h u'")
                    (highlight-regexp re-pattern-czech
                                      'hi-yellow)
                    (goto-char match-point))
                
                (progn
                  ;; return to former point if I don't want to deal with it
                  (message "Alright, no fixing then.")
                  (goto-char return-to-point)))

            ;; return to former point if no illegals found
            (goto-char return-to-point))))))

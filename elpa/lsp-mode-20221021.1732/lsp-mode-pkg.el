(define-package "lsp-mode" "20221021.1732" "LSP mode"
  '((emacs "26.1")
    (dash "2.18.0")
    (f "0.20.0")
    (ht "2.3")
    (spinner "1.7.3")
    (markdown-mode "2.3")
    (lv "0"))
  :commit "a3b3c15359405f442fc51a2db09e503ca3b39f3d" :authors
  '(("Vibhav Pant, Fangrui Song, Ivan Yonchovski"))
  :maintainer
  '("Vibhav Pant, Fangrui Song, Ivan Yonchovski")
  :keywords
  '("languages")
  :url "https://github.com/emacs-lsp/lsp-mode")
;; Local Variables:
;; no-byte-compile: t
;; End:
